<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/list-chat', 'MobileCT@getChats');
Route::get('/list-member', 'MobileCT@getMember');
Route::get('/sent-chat', 'MobileCT@sentChat');
Route::post('/login', 'MobileCT@login');
Route::post('/check-number', 'MobileCT@checkNumber');
Route::post('/register', 'MobileCT@register');
Route::post('/update-profile', 'MobileCT@update');
Route::post('/change-pwd', 'MobileCT@updatePwd');

Route::get('/pengumuman', 'MobileCT@getPengumuman');