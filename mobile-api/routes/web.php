<?php

// Route::get('/', function () {
//     return view('index');
// });
Route::get('/', 'HomeCT@home');
Route::get('/blog', 'HomeCT@blog');
Route::get('/article/{id}/{name}', 'HomeCT@blogDetail');
Route::get('/porto', 'HomeCT@porto');
Route::get('/porto/{page}', 'HomeCT@portoPaginate');

Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('imam', 'ImamCT');
});
Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('kitab', 'KitabCT');
});
Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('bab', 'BabCT');
});
Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('hadits', 'HaditsCT');
});
Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('notif', 'NotifCT');
});

Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('/admin/portofolio', 'PortofolioCT');
});
Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('/admin/blog', 'BlogCT');
});
Route::group(['middleware' => 'checklogin'], function () {
	Route::resource('/admin/team', 'TeamCT');
});

Route::post('/admin/login', 'AdmCT@login');
Route::get('/admin/logout', 'AdmCT@logout');
Route::group(['middleware' => 'checklogin'], function () {
	Route::get('/admin/', 'AdmCT@home');
});

Route::get('/admin/login', function () {
    return view('login');
});