<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortofolioModel extends Model
{
    //
    protected $table = "porto";
    protected $primaryKey = "id";
    public $timestamps = false;
}
