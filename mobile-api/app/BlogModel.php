<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    //
    protected $table = "blog";
    protected $primaryKey = "id";
    public $timestamps = false;
}
