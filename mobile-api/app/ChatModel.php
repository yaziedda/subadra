<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatModel extends Model
{
    protected $table = "chat";
    protected $primaryKey = "id";
    public $timestamps = false;
}
