<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\UserModel;
use App\ChatModel;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use FCM;

class MobileCT extends Controller
{
    //

    public function getChats(Request $request){

    	$offset = $request['page'];
    	
    	$limit = 20;

    	if(!isset($offset)){
    		$offset = 0;
    	}else {
    		$offset = $offset * $limit;
    	}
    	$offset = $offset - $limit;


    	$model = DB::table('chat')
        ->join('user', 'chat.pengirim', '=', 'user.id')
        ->select('chat.*', 'user.nama_lengkap')
        ->orderBy('chat.id', 'desc')
        ->offset($offset)
        ->limit($limit)
        ->get();

        $response['status'] = true;
        $response['data'] = $model;
        return $response;

    }

    public function sentChat(Request $request){

        $userId = $request['user_id'];
        $pesan = $request['pesan'];

        $model = new ChatModel();
        $model->pengirim = $userId;
        $model->penerima = 1;
        $model->pesan = $pesan;

        $model->save();

        $chatData = DB::table('chat')
        ->join('user', 'chat.pengirim', '=', 'user.id')
        ->select('chat.*', 'user.nama_lengkap')
        ->where('chat.id', $model->id)
        ->first();

        $notificationBuilder = new PayloadNotificationBuilder('Subadra Apps');
        $notificationBuilder->setBody($chatData->pesan)
        ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $responseNotif['type'] = 1;
        $responseNotif['data'] = $chatData;

        $dataBuilder->addData(['data' =>  $responseNotif]);

        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $topic = new Topics();
        $topic->topic('subadra');

        $topicResponse = FCM::sendToTopic($topic, null, null, $data);
        // $topicResponse = FCM::sendTo($topic, $option, $notification, $data);

        // return "$topicResponse";
        $topicResponse->isSuccess();
        $topicResponse->shouldRetry();
        $topicResponse->error();

        $response['status'] = true;
        $response['data'] = "Sukses mengirim chat";
        return $responseNotif;

    }

    public function getMember(){

        $model = DB::table('user')
        ->select('user.id', 'user.no_hp', 'user.nama_lengkap', 'user.tanggal_lahir', 'user.photo', 'user.deskripsi')
        ->where('member_id', 1)
        ->get();

        $response['status'] = true;
        $response['data'] = $model;
        return $response;

    }

    public function getPengumuman(){

        $model = DB::table('pengumuman')
        ->join('user', 'pengumuman.pembuat', '=', 'user.id')
        ->select('pengumuman.*', 'user.nama_lengkap')
        // ->where('member_id', 1)
        ->get();

        $response['status'] = true;
        $response['data'] = $model;
        return $response;

    }

    public function login(Request $request){

    	$username = $request['username'];
    	$password = $request['password'];

    	$model = DB::table('user')
        ->select('user.id', 'user.no_hp', 'user.nama_lengkap', 'user.tanggal_lahir', 'user.photo', 'user.deskripsi')
        ->where('no_hp', $username)
        ->where('password', $password)
        ->first();

        if($model != null){
            $response['status'] = true;
            $response['data'] = $model;
        }else{
            $response['status'] = false;
            $response['data'] = null;
        }
        return $response;

    }

    public function checkNumber(Request $request){

        $username = $request['username'];

        $model = DB::table('user')
        ->where('no_hp', $username)
        ->first();

        if($model != null){
            $response['status'] = true;
            $response['data'] = "No HP telah terdaftar";
            return $response;
        }else {
            $response['status'] = false;
            $response['data'] = "No HP belum terdaftar";
            return $response;

        }

    }

    public function register(Request $request){

        $username = $request['username'];
        $nama = $request['nama'];
        $tanggal_lahir = $request['tanggal_lahir'];
        $password = $request['password'];
        $deskripsi = $request['deskripsi'];

        $model = new UserModel();
        $model->member_id = 1;
        $model->no_hp = $username;
        $model->nama_lengkap = $nama;
        $model->tanggal_lahir = $tanggal_lahir;
        $model->password = $password;
        $model->deskripsi = $deskripsi;


        $file = $request->file('file');
        if($file != null){
            $extension   = $file->getClientOriginalExtension();
            $fileName = $username.'_'.Carbon::now()->format('dmYHms').'.'.$extension;
            $request->file('file')->move("storage/", $fileName);
            $model->photo = $fileName;
        }

        $model->save();

        $response['status'] = true;
        $response['data'] = "Sukses registrasi";

        return $response;
    }

    public function update(Request $request){

        $nama = $request['nama'];
        $tanggal_lahir = $request['tanggal_lahir'];
        $password = $request['password'];
        $deskripsi = $request['deskripsi'];

        $model = new UserModel();
        $model = $model->where('id', $request['id']);
        $model->nama_lengkap = $nama;
        $model->tanggal_lahir = $tanggal_lahir;
        $model->deskripsi = $deskripsi;

        $file = $request->file('file');
        if($file != null){
            $extension   = $file->getClientOriginalExtension();
            $fileName = $username.'_'.Carbon::now()->format('dmYHms').'.'.$extension;
            $request->file('file')->move("storage/", $fileName);
            $model->photo = $fileName;
        }

        $model->save();

        $response['status'] = true;
        $response['data'] = "Sukses update profile";
        return $response;
    }

    public function updatePwd(Request $request){

        $userId = $request['id'];
        $pwdOld = $request['password_old'];
        $pwdNew = $request['password_new'];

        $model = new UserModel();
        $model = $model->where('id', $userId);

        if($model->password == $pwdOld){
            $model->password = $pwdNew;
            $model->save();

            $response['status'] = true;
            $response['data'] = "Sukses update password";
            return $response;
        }else {
            $response['status'] = false;
            $response['data'] = "Password lama salah";
            return $response;
        }

    }

}
