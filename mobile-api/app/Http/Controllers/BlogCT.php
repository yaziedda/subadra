<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogModel;
use Carbon\Carbon;

class BlogCT extends Controller
{
    public function index()
    {
    	$model = new BlogModel();
    	$model = $model->all();
        return view('admin.blog.index', compact('model'));
    }

    public function store(Request $request)
    {
        $model = new BlogModel();
        $model->title = $request->title;
        $model->content = $request->content;

        $file       = $request->file('file');
        $extension   = $file->getClientOriginalExtension();
        $fileName = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
        $request->file('file')->move("storage/", $fileName);
        $model->thumbnail = $fileName;

        $file_doc       = $request->file('file_doc');
        $extension   = $file_doc->getClientOriginalExtension();
        $fileNameDoc = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
        $request->file('file_doc')->move("storage/", $fileNameDoc);
        $model->file = $fileNameDoc;


        $model->save();
        return redirect()->route('blog.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function edit($id)
    {
        $model = BlogModel::findOrFail($id);
        return view('admin.blog.edit', compact('model'));
    }

    public function update(Request $request, $id){
    	$model = BlogModel::findOrFail($id);
        $model->title = $request->title;
        $model->content = $request->content;

        $file       = $request->file('file');
        if($file != null){
            $extension   = $file->getClientOriginalExtension();
            $fileName = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
            $request->file('file')->move("storage/", $fileName);
            $model->thumbnail = $fileName;
        }

        $file_doc       = $request->file('file_doc');
        if($file_doc != null){
            $extension   = $file_doc->getClientOriginalExtension();
            $fileNameDoc = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
            $request->file('file_doc')->move("storage/", $fileNameDoc);
            $model->file = $fileNameDoc;
        }

        $model->save();
        return redirect()->route('blog.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function delete($id)
    {
        $data = BlogModel::findOrFail($id);
        return view('blog.delete', compact('data'));
    }

    public function destroy($id)
    {
        $toko = BlogModel::findOrFail($id);
        $toko->delete();
        return redirect()->route('blog.index')->with('alert-success', 'Data Berhasil Hapus.');
    }
}
