<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TeamModel;
use Carbon\Carbon;

class TeamCT extends Controller
{
    //
    public function index()
    {
    	$model = new TeamModel();
    	$model = $model->all();

        // echo asset('storage/file.txt');
        // return $model;

        return view('admin.team.index', compact('model'));
    }

    public function store(Request $request)
    {
        $model = new TeamModel();
        $model->name = $request->name;
        $model->as = $request->as;
        $model->description = $request->description;

        $file       = $request->file('file');
        $extension   = $file->getClientOriginalExtension();
        $fileName = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
        $request->file('file')->move("storage/", $fileName);
        $model->image = $fileName;

        $model->save();
        return redirect()->route('team.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function edit($id)
    {
        $model = TeamModel::findOrFail($id);
        return view('admin.team.edit', compact('model'));
    }

    public function update(Request $request, $id){
    	$model = TeamModel::findOrFail($id);
        $model->name = $request->name;
        $model->as = $request->as;
        $model->description = $request->description;

        $file       = $request->file('file');
        if($file != null){
            $extension   = $file->getClientOriginalExtension();
            $fileName = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
            $request->file('file')->move("storage/", $fileName);
            $model->image = $fileName;
        }
        $model->save();
        return redirect()->route('team.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function delete($id)
    {
        $data = TeamModel::findOrFail($id);
        return view('team.delete', compact('data'));
    }

    public function destroy($id)
    {
        $toko = TeamModel::findOrFail($id);
        $toko->delete();
        return redirect()->route('team.index')->with('alert-success', 'Data Berhasil Hapus.');
    }
}
