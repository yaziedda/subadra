<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdmModel;
use App\PortofolioModel;
use Illuminate\Support\Facades\DB;

class AdmCT extends Controller
{
    public function login(Request $request)
    {
    	$admmodel = new AdmModel();
    	$admmodel = $admmodel->where('username', $request->user_id)->where('password', $request->pkey)->first();
    	if($admmodel != null){
            session(['username' => $admmodel->username]);
            return redirect('/admin/');
        }else{
        	return redirect('/')->with('alert-wrong', 'User ID atau Password Salah');
        }
    	
    }

    public function home(Request $request)
    {
        $model = new PortofolioModel();
        $model = $model->all();
        return view('dashboard', compact('model'));
    }

    public function logout(Request $request)
    {
    	$request->session()->forget('username');
		return redirect('/');
    }
    
}
