<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdmModel;
use App\PortofolioModel;
use App\TeamModel;
use App\BlogModel;
use Illuminate\Support\Facades\DB;

class HomeCT extends Controller
{
    public function login(Request $request)
    {
    	$admmodel = new AdmModel();
    	$admmodel = $admmodel->where('username', $request->user_id)->where('password', $request->pkey)->first();
    	if($admmodel != null){
            session(['username' => $admmodel->username]);
            return redirect('/admin/');
        }else{
        	return redirect('/')->with('alert-wrong', 'User ID atau Password Salah');
        }
    	
    }

    public function home(Request $request)
    {
        $porto = new PortofolioModel();
        $porto = $porto->limit(5)->orderBy('id','desc')->get();
        $team = new TeamModel();
        $team = $team->all();
        $blog = new BlogModel();
        $blog = $blog->all();

        $model['porto'] = $porto;
        $model['team'] = $team;
        $model['blog'] = $blog;
        return view('index_n', compact('model'));
    }

    public function blog(Request $request)
    {
        $model = new BlogModel();
        $model = $model->all();
        return view('blog_n', compact('model'));
    }

    public function porto(Request $request)
    {
        $model = new PortofolioModel();
        $model = $model->all();
        return view('porto_n', compact('model'));

        // $count = DB::table('porto')->get();
        
        // $model = DB::table('porto')
        //             ->offset(0)
        //             ->limit(10)
        //             ->get();
        // $data['count'] = round($count->count()/10);
        // $data['model'] = $model;

        // return $data;
    }

    public function portoPaginate($page)
    {
        // $model = new PortofolioModel();
        // $model = $model->all();
        // return view('porto', compact('model'));

        $model = DB::table('porto')
                    ->offset($page)
                    ->limit(10)
                    ->get();
        // return $model->count();
        return $model;
    }

    public function blogDetail($id) {

        $model = new BlogModel();
        $model = $model->where('id', $id)->first();
        return view('blog-detail_n', compact('model'));
        
    }



    public function logout(Request $request)
    {
    	$request->session()->forget('username');
		return redirect('/');
    }
    
}
