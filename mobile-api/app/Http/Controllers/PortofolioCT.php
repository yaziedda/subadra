<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PortofolioModel;
use Carbon\Carbon;

class PortofolioCT extends Controller
{
    //
    public function index()
    {
    	$model = new PortofolioModel();
    	$model = $model->all();

        // echo asset('storage/file.txt');
        // return $model;

        return view('admin.portofolio.index', compact('model'));
    }

    public function store(Request $request)
    {
        $model = new PortofolioModel();
        $model->title = $request->title;
        $model->description = $request->description;

        $file       = $request->file('file');
        $extension   = $file->getClientOriginalExtension();
        $fileName = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
        $request->file('file')->move("storage/", $fileName);
        $model->image = $fileName;

        // return $file;

        $model->save();
        return redirect()->route('portofolio.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function edit($id)
    {
        $model = PortofolioModel::findOrFail($id);
        return view('admin.portofolio.edit', compact('model'));
    }

    public function update(Request $request, $id){
    	$model = PortofolioModel::findOrFail($id);
        $model->title = $request->title;
        $model->description = $request->description;

        $file       = $request->file('file');
        if($file != null){
            $extension   = $file->getClientOriginalExtension();
            $fileName = str_replace(' ', '', $request->title).'_'.Carbon::now()->format('dmYHms').'.'.$extension;
            $request->file('file')->move("storage/", $fileName);
            $model->image = $fileName;
        }
        $model->save();
        return redirect()->route('portofolio.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function delete($id)
    {
        $data = PortofolioModel::findOrFail($id);
        return view('portofolio.delete', compact('data'));
    }

    public function destroy($id)
    {
        $toko = PortofolioModel::findOrFail($id);
        $toko->delete();
        return redirect()->route('portofolio.index')->with('alert-success', 'Data Berhasil Hapus.');
    }
}
