
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>BITSKYLAB</title>

  <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}">

  <link href="{{ URL::asset('dist/css/bootstrap.min.css') }}" rel="stylesheet">

  <link href="{{ URL::asset('assets/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

  <script src="{{ URL::asset('assets/js/ie-emulation-modes-warning.js') }}"></script>

  <link href="{{ URL::asset('assets/css/carousel.css') }}" rel="stylesheet">

  <link href="{{ URL::asset('assets/css/main.css') }}" rel="stylesheet">

  <script src="{{ URL::asset('assets/js/wow.js') }}"></script>
  <script>
    new WOW().init();
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<!-- NAVBAR
  ================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="{{ URL::asset('assets/img/logofulw.png') }}" height="60px"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="/#ourservices">Our Services</a></li>
                <li><a href="/#ourwork">Our Works</a></li>
                <li><a href="/blog">Blog</a></li>
                <li><a href="/#contact">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="{{ URL::asset('assets/img/backbit.png') }}" >
          <div class="container">
            <div class="carousel-caption">
              <div class="row">
                <div class="col-md-5 wow fadeInLeft" data-wow-delay=".5s">
                  <h1>CREATIVE</h1>
                  <h1>COMPETENCE</h1>
                  <h1>PROFESSIONAL</h1>
                  <p>IT Solution and Course</p>
                  <p><a href="#"><button class="btn-light-pink">IN KARAWANG</button></a></p>
                </div>
                <div class="col-md-7 wow fadeIn" data-wow-delay="1s">
                  <div class="image-intro">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="{{ URL::asset('assets/img/backbit4.png') }}">
          <div class="container">
            <div class="carousel-caption">
              <div class="row">
                <div class="col-md-5 wow fadeInLeft" data-wow-delay=".5s">
                  <h1>ANOTHER</h1>
                  <h1>EXAMPLE</h1>
                  <h1>HEADLINE</h1>
                  <p>Cras justo odio, dapibus</p>
                  <p><a href="#"><button class="btn-light-orange">GET STARTED</button></a></p>
                </div>
                <div class="col-md-7 wow fadeIn" data-wow-delay="1s">
                  <div class="image-intro1">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="{{ URL::asset('assets/img/backbit3.png') }}">
          <div class="container">
            <div class="carousel-caption">
              <div class="row">
                <div class="col-md-5 wow fadeInLeft" data-wow-delay=".5s">
                  <h1>ONE MORE</h1>
                  <h1>FOR GOOD</h1>
                  <h1>MEASURE</h1>
                  <p>Donec id elit non mi porta</p>
                  <p><a href="#"><button class="btn-light-blue">READ MORE</button></a></p>
                </div>
                <div class="col-md-7 wow fadeIn" data-wow-delay="1s">
                  <div class="image-intro2">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.carousel -->

    <div class="container">
      <div id="ourservices"><br><br>
        <div class="row">
          <div class="col-lg-6">
            <h1 class="header-title-what wow fadeInDown">OUR SERVICES</h1>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-6">
            <div class="services-features">
              <div class="row wow fadeInUp">
                <div class="col-md-3">
                  <img src="{{ URL::asset('assets/img/webapp.png') }}" height="120px">
                </div>
                <div class="col-md-9">
                  <h3>Web App</h3>
                  Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae
                </div>
              </div>
              <div class="row wow fadeInUp" data-wow-delay=".5s">
                <div class="col-md-3">
                  <img src="{{ URL::asset('assets/img/mobileapp.png') }}" height="120px">
                </div>
                <div class="col-md-9">
                  <h3>Mobile App</h3>
                  Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae
                </div>
              </div>
              <div class="row wow fadeInUp" data-wow-delay="1s">
                <div class="col-md-3">
                  <img src="{{ URL::asset('assets/img/course.png') }}" height="120px">
                </div>
                <div class="col-md-9">
                  <h3>IT Course</h3>
                  Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae
                </div>
              </div>
            </div>
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
        <!-- START THE FEATURETTES -->
      </div>
    </div>
    <div id="ourwork" class="container">
      <hr class="featurette-divider">
      <center><h1 class="header-title">OUR WORK</h1><br><Br></center>

      <?php $pos=1 ?>
      @foreach($model['porto'] as $item)
      @if(($pos+1) % 2 === 0) 

      <div class="row featurette wow fadeIn">
        <div class="col-md-7">
          <h2 class="featurette-heading">
            {{$item->title}} 
          <!-- <span class="text-muted">It'll blow your mind.</span>< -->
          </h2>
          <p class="lead">{{$item->description}}</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" src="/storage/{{$item->image}}" alt="{{$item->title}}">
        </div>
      </div>

      <hr class="featurette-divider">
      @elseif(count($item->id) % 2 === 1)

      <div class="row featurette wow fadeIn">
        <div class="col-md-7 col-md-push-5">
          <h2 class="featurette-heading">
            {{$item->title}} 
            <!-- <span class="text-muted">See for yourself.</span> -->
          </h2>
          <p class="lead">{{$item->description}}</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" src="/storage/{{$item->image}}" alt="{{$item->title}}">
        </div>
      </div>

      <hr class="featurette-divider">
      @endif
      <?php $pos++ ?>
      @endforeach

      <br>
      <p class="text-right" style="font-size: 20px;">
        <a href="/porto">See More..</a>
      </p>

    </div>


    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->

    <div class="container marketing">
      <div id="ourteam">
        <center><h1 class="header-title">OUR TEAM</h1></center>
        <br>
        <!-- Three columns of text below the carousel -->
        <div class="row">
          @foreach($model['team'] as $item)
          <div class="col-lg-4 wow fadeIn">
            <img class="img-circle" src="/storage/{{$item->image}}" alt="Generic placeholder image" width="140" height="140">
            <h4>{{$item->name}}</h4>
            <h5>{{$item->as}}</h5>
            <p>{{$item->description}}</p>
            <!-- <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p> -->
          </div><!-- /.col-lg-4 -->
          @endforeach
        </div><!-- /.row -->
      </div>

      <div class="container marketing">
        <div id="contact">
          <center><h1 class="header-title">CONTACT</h1></center>
          <br>
          <!-- Three columns of text below the carousel -->
          <div class="row">
            <div class="col-lg-12">
              <center>
                <form id="formcontact" class="wow fadeInUp">
                  <input type="text" class="inputcontact" name="name" placeholder="Your Name ...">
                  <br>
                  <input type="text" class="inputcontact" name="email" placeholder="Your Email ...">
                  <br>
                  <textarea class="inputcontact"></textarea>
                  <br>
                  <button class="btn-light-pink">SUBMIT</button>
                </form>
              </center>
            </div><!-- /.col-lg-4 -->
          </div><!-- /.row -->
        </div>

        <!-- FOOTER -->
        <footer>
          <p>&copy; 2017 Bitskylab Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>
        <a href="#" id="back-to-top" title="Back to top">&uarr;</a>

      </div><!-- /.container -->

      <script>window.jQuery || document.write('<script src="{{ URL::asset("assets/js/vendor/jquery.min.js") }}"><\/script>')</script>
      <script src="{{ URL::asset('dist/js/bootstrap.min.js') }}"></script>
      <script src="{{ URL::asset('assets/js/vendor/holder.min.js') }}"></script>
      <script src="{{ URL::asset('assets/js/ie10-viewport-bug-workaround.js') }}"></script>
      <script type="text/javascript">
        var $item = $('.carousel .item'); 
        var $wHeight = $(window).height();
        $item.eq(0).addClass('active');
        $item.height($wHeight); 
        $item.addClass('full-screen');

        $('.carousel img').each(function() {
          var $src = $(this).attr('src');
          var $color = $(this).attr('data-color');
          $(this).parent().css({
            'background-image' : 'url(' + $src + ')',
            'background-color' : $color
          });
          $(this).remove();
        });

        $(window).on('resize', function (){
          $wHeight = $(window).height();
          $item.height($wHeight);
        });

        $('.carousel').carousel({
          interval: 6000,
          pause: "false"
        });
      </script>
      <script type="text/javascript">
        $(document).ready(function(){
          $('body').scrollspy({target: ".navbar", offset: 50});   
          $("#navbar a").on('click', function(event) {
            if (this.hash !== "") {
              event.preventDefault();
              var hash = this.hash;
              $('html, body').animate({
                scrollTop: $(hash).offset().top
              }, 800, function(){
                window.location.hash = hash;
              });
            });
        });
      </script>
      <script type="text/javascript">
        if ($('#back-to-top').length) {
          var scrollTrigger = 100, // px
          backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
              $('#back-to-top').addClass('show');
            } else {
              $('#back-to-top').removeClass('show');
            }
          };
          backToTop();
          $(window).on('scroll', function () {
            backToTop();
          });
          $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
              scrollTop: 0
            }, 700);
          });
        }
      </script>
    </body>
    </html>
