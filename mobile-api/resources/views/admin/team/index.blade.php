@extends('dashboard')
@section('content')
<section class="content-header">
	<h1>
		Team 
		<small>View All</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-calendar"></i> Team </a></li>
		<li class="active">View All</li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Input Team </h3>
		</div>
		<form action="{{route('team.store')}}" method="post" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="box-body">
				
				<div class="form-group">
					<label>Name </label>
					<input type="text" class="form-control" placeholder="Name " name="name">
				</div>
				<div class="form-group">
					<label>As / Position </label>
					<input type="text" class="form-control" placeholder="As / Position " name="as">
				</div>
				<div class="form-group">
					<label>Description </label>
					<textarea class="form-control" name="description" rows="4"></textarea>
				</div>
				<div class="form-group">
					<label>Image</label>
					<input type="file" class="form-control" name="file">
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		<div class="box-body">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<table id="table" class="table table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Id</th>
									<th>Image</th>
									<th>Name</th>
									<th>As / Position</th>
									<th>Description</th>
									<th>Created At</th>
									<th>Update At</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($model as $item)
								<tr>
									<td>{{$item->id}}</td>
									<td><b><img src="/storage/{{$item->image}}" width="150"></b>
									<td><b>{{$item->name}}</b>
									<td><b>{{$item->as}}</b>
									<td style="max-width: 400px; overflow: hidden;word-wrap: break-word;">
										{{$item->description}}
									</td>
									<td>{{$item->created_at}}</td>
									<td>{{$item->updated_at}}</td>
									<td><a href="{{route('team.edit', $item->id)}}" class="btn btn-primary" data-toggle="modal" data-target="#modal_edit" data-id="{{$item->id}}" >Edit</a> <a href="#modal_delete'" class="btn btn-danger" data-toggle="modal" data-target="#modal_delete_{{$item->id}}" data-id="{{$item->id}}" >Delete</a></td>
								</tr>
								<div class="modal fade" id="modal_delete_{{$item->id}}">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Hapus Data</h4>
											</div>
											<form action="{{route('team.destroy', $item->id)}}" method="post" id="form_delete" accept-charset="UTF-8">
												<input name="_method" type="hidden" value="DELETE">
												<input name="_token" type="hidden" value="{{ csrf_token() }}">
												<div class="modal-body">
													<h4 class="modal-title">Apakah anda yakin akan menghapus?</h4>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-danger">Ya</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_edit" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript">
	 $(document).ready(function(){
      $("#modal_edit").on("hidden.bs.modal", function(){
        $(this).removeData('bs.modal');
      });
      $("#modal_delete").on("hidden.bs.modal", function(){
        $(this).removeData('bs.modal');
      });
      $('#table').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : false
		})
    });
</script>
@endsection