<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Edit Portofolio</h4>
</div>
<form action="{{route('portofolio.update', $model->id)}}" method="post" enctype="multipart/form-data">
  <input name="_method" type="hidden" value="PATCH">
  {{csrf_field()}}
  <div class="modal-body">
    <div class="form-group">
      <label>Title </label>
      <input type="text" class="form-control" placeholder="Title " name="title" value="{{$model->title}}">
    </div>
    <div class="form-group">
      <label>Description </label>
      <textarea class="form-control" name="description" rows="4">{{$model->description}}</textarea>
    </div>
    <div class="form-group">
      <img src="/storage/{{$model->image}}" width="150"><br><br>
      <input type="file" class="form-control" placeholder="Nama Portofolio " name="file">
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</form>
