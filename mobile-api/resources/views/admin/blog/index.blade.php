@extends('dashboard')
@section('content')
<script src="{{ URL::asset('assets/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/ckeditor/samples/js/sample.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css')}}">
<section class="content-header">
	<h1>
		Blog 
		<small>View All</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-calendar"></i> Blog </a></li>
		<li class="active">View All</li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Input Blog </h3>
		</div>
		<form action="{{route('blog.store')}}" method="post" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="box-body">
				
				<div class="form-group">
					<label>Title </label>
					<input type="text" class="form-control" placeholder="Title " name="title">
				</div>
				<div class="form-group">
					<label>Thumbnail</label>
					<input type="file" class="form-control" name="file">
				</div>
				<div class="form-group">
					<label>File Document</label>
					<input type="file" class="form-control" name="file_doc">
				</div>
				<div class="form-group">
					<label>Content</label>
					<textarea id="editor" name="content"></textarea>
					<script>
						initSample();
					</script>

				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		<div class="box-body">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<table id="table" class="table table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Id</th>
									<th>Thumbnail</th>
									<th>Title</th>
									<th>Content</th>
									<th>File</th>
									<th>Created At</th>
									<th>Update At</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($model as $item)
								<tr>
									<td>{{$item->id}}</td>
									<td><img src="/storage/{{$item->thumbnail}}" width="70"><br><br></td>
									<td><b>{{$item->title}}</b></td>
									<td style="max-width: 400px; overflow: hidden;word-wrap: break-word;">{{$item->content}}</td>
									<td><a href="/storage/{{$item->file}}" target="blank">{{$item->file}}</a></td>
									<td>{{$item->created_at}}</td>
									<td>{{$item->updated_at}}</td>
									<td><a href="{{route('blog.edit', $item->id)}}" class="btn btn-primary">Edit</a> <a href="#modal_delete'" class="btn btn-danger" data-toggle="modal" data-target="#modal_delete_{{$item->id}}" data-id="{{$item->id}}" >Delete</a></td>
								</tr>
								<div class="modal fade" id="modal_delete_{{$item->id}}">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Hapus Data</h4>
											</div>
											<form action="{{route('blog.destroy', $item->id)}}" method="post" id="form_delete" accept-charset="UTF-8">
												<input name="_method" type="hidden" value="DELETE">
												<input name="_token" type="hidden" value="{{ csrf_token() }}">
												<div class="modal-body">
													<h4 class="modal-title">Apakah anda yakin akan menghapus?</h4>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-danger">Ya</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_edit" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#modal_edit").on("hidden.bs.modal", function(){
			$(this).removeData('bs.modal');
		});
		$("#modal_delete").on("hidden.bs.modal", function(){
			$(this).removeData('bs.modal');
		});
		$('#table').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : false
		})
	});
</script>
@endsection