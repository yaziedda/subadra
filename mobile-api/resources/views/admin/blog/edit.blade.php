@extends('dashboard')
@section('content')
<script src="{{ URL::asset('assets/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/ckeditor/samples/js/sample.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css')}}">
<section class="content-header">
  <h1>
    Blog 
    <small>View All</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-calendar"></i> Blog </a></li>
    <li class="active">View All</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Blog </h3>
    </div>
    <form action="{{route('blog.update', $model->id)}}" method="post" enctype="multipart/form-data">
      <input name="_method" type="hidden" value="PATCH">
      {{csrf_field()}}
      <div class="modal-body">
        <div class="form-group">
          <label>Title </label>
          <input type="text" class="form-control" placeholder="Title " name="title" value="{{$model->title}}">
        </div>
        <div class="form-group">
          <label>Thumbnail</label><br>
          <a href="/storage/{{$model->thumbnail}}" target="blank">
            <img src="/storage/{{$model->thumbnail}}" width="150"><br><br>
          </a>
          <input type="file" class="form-control" name="file">
        </div>
        <div class="form-group">
          <label>File Document</label><br>
          <a href="/storage/{{$model->file}}" target="blank">{{$model->file}}</a>
          <br><br>
          <input type="file" class="form-control" name="file_doc">
        </div>
        <div class="form-group">
          <label>Content</label>
          <textarea id="editor" name="content">{{$model->content}}</textarea>
          <script>
            initSample();
          </script>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/admin/blog"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></a>
      </div>
    </form>
  </div>
</section>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#modal_edit").on("hidden.bs.modal", function(){
      $(this).removeData('bs.modal');
    });
    $("#modal_delete").on("hidden.bs.modal", function(){
      $(this).removeData('bs.modal');
    });
    $('#table').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  });
</script>
@endsection