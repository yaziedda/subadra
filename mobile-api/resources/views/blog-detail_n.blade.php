
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>BITSKYLAB</title>

  <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css')}}">

  <link href="{{ URL::asset('dist/css/bootstrap.min.css')}}" rel="stylesheet">

  <link href="{{ URL::asset('assets/css/ie10-viewport-bug-workaround.css')}}" rel="stylesheet">

  <script src="{{ URL::asset('assets/js/ie-emulation-modes-warning.js')}}"></script>

  <link href="{{ URL::asset('assets/css/carousel.css')}}" rel="stylesheet">

  <link href="{{ URL::asset('assets/css/main.css')}}" rel="stylesheet">

  <script src="{{ URL::asset('assets/js/wow.js')}}"></script>
  <script>
    new WOW().init();
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<!-- NAVBAR
  ================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="{{ URL::asset('assets/img/logofulw.png')}}" height="60px"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Home</a></li>
                <li><a href="/#ourservices">Our Services</a></li>
                <li><a href="/#ourwork">Our Works</a></li>
                <li class="active"><a href="/blog">Blog</a></li>
                <li><a href="/#contact">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>

    <div class="header-blog">
    </div>


    <div class="container marketing">

      <h1><a href="/article/{{$model->id}}/{{$model->title}}">{{$model->title}}</a></h1>
      {{$model->created_at}}

      <div class="row" style="padding: 20px">
        {!!$model->content!!}
      </div><!-- /.row -->

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->

      <hr class="featurette-divider">
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2016 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->


    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{{ URL::asset('dist/js/bootstrap.min.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{ URL::asset('assets/js/vendor/holder.min.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ URL::asset('assets/js/ie10-viewport-bug-workaround.js')}}"></script>
  </body>
  </html>
