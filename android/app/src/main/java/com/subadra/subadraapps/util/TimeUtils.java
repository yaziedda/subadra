package com.subadra.subadraapps.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeUtils {

    public static ArrayList<String> getArrBulan(){
        ArrayList<String> arrBulan = new ArrayList<>();

        arrBulan.add("Jan");
        arrBulan.add("Feb");
        arrBulan.add("Mar");
        arrBulan.add("Apr");
        arrBulan.add("Mei");
        arrBulan.add("Jun");
        arrBulan.add("Jul");
        arrBulan.add("Agu");
        arrBulan.add("Sep");
        arrBulan.add("Okt");
        arrBulan.add("Nov");
        arrBulan.add("Des");

        return arrBulan;
    }

    public static String convertDate(String tgl){

        ArrayList<String> bulan = getArrBulan();
        String date = tgl;

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date past = format.parse(tgl);
            Date now = new Date();

            if(TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime()) > 60 ) {
                if(TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) > 60 ) {
                    if(TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) > 24 ) {

                        if(TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) > 7 ) {

                            String[] temp1 = tgl.split(" ");
                            String[] temp2 = temp1[0].split("-");
                            tgl = temp2[2]+" "+bulan.get(Integer.parseInt(temp2[1])-1)+" "+temp2[0];
                            Log.d("TANGGAL", temp2[2]+" "+temp2[1]+" "+temp2[0]);
                            Log.d("TANGGAL", tgl);

                            return tgl;

                        } else {
                            tgl = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) + "d";
                            System.out.println(TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) + "d");

                            return tgl;
                        }


                    } else {
                        tgl = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) + "h";
                        System.out.println(TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) + "h");
                        return tgl;
                    }
                } else {
                    tgl = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) + "m";
                    System.out.println(TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) + "m");
                    return tgl;
                }
            } else {
                tgl = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime()) + "s";
                System.out.println(TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime()) + "s");
                if(tgl.contains("-")){
                    tgl = "0";
                }else if(tgl.equals("0s")){
                    tgl = "Baru saja";
                }
                return tgl;
            }
        }
        catch (Exception j){
            j.printStackTrace();
            return tgl;
        }
    }

}
