package com.subadra.subadraapps.ui.pengumuman;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PengumumanModel implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pembuat")
    @Expose
    private Integer pembuat;
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("konten")
    @Expose
    private String konten;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("nama_lengkap")
    @Expose
    private String namaLengkap;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPembuat() {
        return pembuat;
    }

    public void setPembuat(Integer pembuat) {
        this.pembuat = pembuat;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getKonten() {
        return konten;
    }

    public void setKonten(String konten) {
        this.konten = konten;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

}
