package com.subadra.subadraapps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.subadra.subadraapps.ui.LoginActivity;
import com.subadra.subadraapps.util.Preferences;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {

                    // check apakah sudah login
                    if (Preferences.getLoginFlag(getApplicationContext())){
                        // jika sudah login menuju ke menu utama
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        finish();
                    }else{

                        // jika belum login menuju ke menu login
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                    }
                }
            }
        }).start();
    }

    @Override
    public void onBackPressed() {

    }
}
