package com.subadra.subadraapps.ui.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.util.Static;

import java.util.ArrayList;
import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatHolder>{

    List<ChatModel> list = new ArrayList<>();
    Context context;

    public ChatAdapter(Context context, List<ChatModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ChatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Static.CHAT_SENDER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_chat_sender, parent, false);
                return new ChatHolder(view, viewType);
            case Static.CHAT_RECEIVER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_chat_receiver, parent, false);
                return new ChatHolder(view, viewType);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_chat_sender, parent, false);
                return new ChatHolder(view, viewType);
        }
    }

    @Override
    public void onBindViewHolder(ChatHolder holder, int position) {
        ChatModel chatModel = list.get(position);

        switch (chatModel.getTipe()){
            case Static.CHAT_SENDER:
                holder.tvContent.setText(chatModel.getPesan());
                holder.tvWaktu.setText(chatModel.getTanggal());
                if(chatModel.getStatus() == 0){
//                    holder.ivStatus.setVisibility(View.INVISIBLE);
                }else if (chatModel.getStatus() == 1){
//                    holder.ivStatus.setImageResource(R.mipmap.ic_loading);
                }
                holder.tvName.setText(chatModel.getNamaLengkap());
                break;
            case Static.CHAT_RECEIVER:
                holder.tvContent.setText(chatModel.getPesan());
                holder.tvWaktu.setText(chatModel.getTanggal());
                holder.tvName.setText(chatModel.getNamaLengkap());
//                Glide.with(context).load().apply(new RequestOptions().centerCrop()).into(holder.ivImage);
                holder.ivImage.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getTypeMessage(context);
    }

    public class ChatHolder extends RecyclerView.ViewHolder {

        TextView tvContent;
        TextView tvName;
        TextView tvWaktu;
        ImageView ivStatus;
        ImageView ivImage;

        public ChatHolder(View itemView, int type) {
            super(itemView);
            switch (type){
                case Static.CHAT_SENDER:
                    tvName = (TextView) itemView.findViewById(R.id.tv_name);
                    tvContent = (TextView) itemView.findViewById(R.id.tv_content);
                    tvWaktu = (TextView) itemView.findViewById(R.id.tv_waktu);
                    ivStatus = (ImageView) itemView.findViewById(R.id.iv_status);
                    break;
                case Static.CHAT_RECEIVER:
                    tvName = (TextView) itemView.findViewById(R.id.tv_name);
                    tvContent = (TextView) itemView.findViewById(R.id.tv_content);
                    tvWaktu = (TextView) itemView.findViewById(R.id.tv_waktu);
                    ivImage = (ImageView) itemView.findViewById(R.id.iv_profile);
                    break;
            }
        }
    }

}
