package com.subadra.subadraapps.ui.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.ApiUtils;
import com.subadra.subadraapps.client.chat.ChatService;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.util.Preferences;
import com.subadra.subadraapps.util.Static;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.iv_sent)
    ImageView ivSent;

    ChatService chatService;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    int PAGE = 1;

    List<ChatModel> list = new ArrayList<>();
    boolean loading = false;
    @BindView(R.id.et_content_chat)
    EditText etContentChat;
    int ID_TEMP_CHAT = -0;
    ChatAdapter chatAdapter;
    LinearLayoutManager layoutManager;
    public static final  String BROADCAST_CHAT = "BROADCAST_CHAT";

    BroadcastReceiver chatReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("broadcast receive");
            if(intent.getAction().equals(BROADCAST_CHAT)){
                ChatModel chatModel = (ChatModel) intent.getSerializableExtra(ChatModel.class.getName());
                list.add(chatModel);
                initAdapter();
            }
        }
    };

    IntentFilter intentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        setTitle("Subadra 2019");


        intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction("android.intent.action.LOCKED_BOOT_COMPLETED");
        intentFilter.addAction(BROADCAST_CHAT);

        chatService = ApiUtils.ChatService(getApplicationContext());
        initChat();
        ivSent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pesan = etContentChat.getText().toString();
                if(!pesan.isEmpty()){

                    User user = Preferences.getUser(getApplicationContext());
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("user_id", String.valueOf(user.getId()));
                    map.put("pesan", pesan);
                    ID_TEMP_CHAT = ID_TEMP_CHAT+1;


                    ChatModel chatModel = new ChatModel();
                    chatModel.setId(ID_TEMP_CHAT);
                    chatModel.setPesan(pesan);
                    chatModel.setPengirim(user.getId());
                    chatModel.setPenerima(1);
                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    Date today = Calendar.getInstance().getTime();
                    String date = df.format(today);
                    chatModel.setTanggal(date);
                    chatModel.setTipe(Static.CHAT_SENDER);
                    chatModel.setNamaLengkap(user.getNamaLengkap());
                    list.add(chatModel);
                    initAdapter();
//                    layoutManager.setStackFromEnd(true);
                    etContentChat.setText("");
                    chatService.sentChat(map).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if(response.isSuccessful()){
                                Response body = response.body();
                                if(body.isStatus()){

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                        }
                    });
                }else{
                    Toast.makeText(getApplicationContext(), "Tidak bisa mengirim chat kosong", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(chatReceiver, intentFilter);
    }

    private void initChat() {
        pbLoading.setVisibility(View.VISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("page", String.valueOf(PAGE));
        chatService.listChat(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                pbLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.isStatus()) {
                        if (body.getData() != null) {
                            Gson gson = new Gson();
                            JsonObject jsonObject = gson.toJsonTree(response.body()).getAsJsonObject();
                            List<ChatModel> listMerchant = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<ChatModel>>() {
                            }.getType());
                            list.addAll(listMerchant);
                            if (list.size() > 0) {
                                Collections.reverse(list);
                                initAdapter();
//                                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                                    @Override
//                                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                                        System.out.println("dy : "+dy);
//                                        if (dy < 0) {
//                                            int pastVisiblesItems, visibleItemCount, totalItemCount;
//                                            visibleItemCount = layoutManager.getChildCount();
//                                            totalItemCount = layoutManager.getItemCount();
//                                            pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
//                                            if (!loading) {
//                                                System.out.println("visibleItemCount : "+visibleItemCount);
//                                                System.out.println("totalItemCount : "+totalItemCount);
//                                                System.out.println("pastVisiblesItems : "+pastVisiblesItems);
//                                                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                                                    loading = true;
//                                                    PAGE = PAGE + 1;
//                                                    pagging();
//                                                    System.out.println("pagging");
//                                                }
//                                            }
//                                        }
//                                    }
//                                });
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                pbLoading.setVisibility(View.GONE);
            }
        });
    }

    private void initAdapter() {
        chatAdapter = new ChatAdapter(getApplicationContext(), list);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(chatAdapter);
    }

    private void pagging() {
        loading = true;
        pbLoading.setVisibility(View.VISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("page", String.valueOf(PAGE));
        chatService.listChat(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                pbLoading.setVisibility(View.GONE);
                loading = false;
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.isStatus()) {
                        if (body.getData() != null) {
                            Gson gson = new Gson();
                            JsonObject jsonObject = gson.toJsonTree(response.body()).getAsJsonObject();
                            List<ChatModel> listMerchant = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<ChatModel>>() {
                            }.getType());
                            Collections.reverse(list);
                            list.addAll(listMerchant);
                            Collections.reverse(list);
                            chatAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                loading = false;
                pbLoading.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(chatReceiver);
        super.onDestroy();
    }
}
