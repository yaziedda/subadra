package com.subadra.subadraapps;

import com.google.firebase.messaging.FirebaseMessaging;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseMessaging.getInstance().subscribeToTopic("subadra");
    }
}
