package com.subadra.subadraapps.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.subadra.subadraapps.client.ApiUtils;
import com.subadra.subadraapps.client.chat.ChatService;

import java.util.Map;



public class BaseActivity extends AppCompatActivity{

    private static Context context;
    private MaterialDialog materialDialogPleasewait;
    public ChatService mobileService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        mobileService = ApiUtils.ChatService(getApplicationContext());

        materialDialogPleasewait = new MaterialDialog.Builder(BaseActivity.this)
                .content(Static.DIALOG_PLEASEWAIT_TITLE)
                .progress(true, 0)
                .cancelable(false)
                .build();
    }

    public void showMessage(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public void showSnackbar(String message){
        Snackbar.make(getCurrentFocus(), message, Snackbar.LENGTH_SHORT).show();
    }

    public void showActivity(Context packageContext, Class<?> cls) {
        Intent intent = getIntent(packageContext, cls);
        showActivity(intent);
    }

    public Intent getIntent(Context packageContext, Class<?> cls) {
        return new Intent(packageContext, cls);
    }

    public void showActivity(Intent intent) {
        startActivity(intent);
//        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
//        overridePendingTransition(R.anim.push_out_right, R.anim.pull_in_left);
    }

    public void showActivityAndFinishCurrentActivity(Context packageContext, Class<?> cls) {
        Intent intent = getIntent(packageContext, cls);
        showActivity(intent);
        finish();
    }

    public void showPleasewaitDialog(){
        try {
            if(materialDialogPleasewait != null && this != null && !this.isFinishing()){
                materialDialogPleasewait.show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void dissmissPleasewaitDialog(){
        try {
            if(materialDialogPleasewait != null && this != null && !this.isFinishing()){
                materialDialogPleasewait.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
