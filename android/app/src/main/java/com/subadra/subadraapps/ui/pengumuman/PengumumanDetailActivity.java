package com.subadra.subadraapps.ui.pengumuman;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.subadra.subadraapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PengumumanDetailActivity extends AppCompatActivity {

    PengumumanModel pengumumanModel;
    @BindView(R.id.tv_judul)
    TextView tvJudul;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_content)
    TextView tvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengumuman_detail);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pengumumanModel = (PengumumanModel) getIntent().getSerializableExtra("pengumumanModel");
        setTitle(pengumumanModel.getJudul());

        tvJudul.setText(pengumumanModel.getJudul());
        tvTime.setText(pengumumanModel.getTanggal());
        tvContent.setText(pengumumanModel.getKonten());


    }
}
