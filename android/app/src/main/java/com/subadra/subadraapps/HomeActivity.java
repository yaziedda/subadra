package com.subadra.subadraapps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.subadra.subadraapps.ui.LoginActivity;
import com.subadra.subadraapps.ui.VersiActivity;
import com.subadra.subadraapps.ui.chat.ChatActivity;
import com.subadra.subadraapps.ui.member.MemberActivity;
import com.subadra.subadraapps.ui.pengumuman.PengumumanActivity;
import com.subadra.subadraapps.ui.profile.ProfileActivity;
import com.subadra.subadraapps.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.ll_chat)
    LinearLayout llChat;
    @BindView(R.id.ll_member)
    LinearLayout llMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ll_chat, R.id.ll_member, R.id.ll_pengumuman, R.id.ll_profile, R.id.ll_info, R.id.ll_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_chat:
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
                break;
            case R.id.ll_member:
                startActivity(new Intent(getApplicationContext(), MemberActivity.class));
                break;
            case R.id.ll_pengumuman:
                startActivity(new Intent(getApplicationContext(), PengumumanActivity.class));
                break;
            case R.id.ll_profile:
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            case R.id.ll_info:
                startActivity(new Intent(getApplicationContext(), VersiActivity.class));
                break;
            case R.id.ll_logout:
                new MaterialDialog.Builder(HomeActivity.this)
                        .title("Peringatan")
                        .content("Apakah anda yakin akan logout?")
                        .positiveText("Ya")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                                Preferences.setLoginFlag(getApplicationContext(), false);
                                Preferences.setUser(getApplicationContext(), null);
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        })
                        .negativeText("Tidak")
                        .show();
                break;
        }
    }
}
