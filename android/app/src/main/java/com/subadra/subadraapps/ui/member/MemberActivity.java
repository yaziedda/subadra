package com.subadra.subadraapps.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.ApiUtils;
import com.subadra.subadraapps.client.chat.ChatService;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.ui.chat.ChatActivity;
import com.subadra.subadraapps.util.Static;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class MemberActivity extends AppCompatActivity {


    MaterialDialog materialDialog;
    ChatService chatService;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.bt_chat)
    Button btChat;
    @BindView(R.id.bt_anggota)
    Button btAnggota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Member");

        chatService = ApiUtils.ChatService(getApplicationContext());
        materialDialog = new MaterialDialog.Builder(this)
                .content("Loading..")
                .cancelable(false)
                .progress(true, 0)
                .show();
        btAnggota.setText("");

        chatService.getMember().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                materialDialog.dismiss();
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.isStatus()) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(response.body()).getAsJsonObject();
                        List<MemberModel> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MemberModel>>() {
                        }.getType());

                        btAnggota.setText(listBody.size()+" anggota");

                        MemberAdapter adapter = new MemberAdapter(getApplicationContext(), listBody, new MemberAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(MemberModel model) {

                            }
                        });
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), Static.ERR_MSG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Static.ERR_MSG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                materialDialog.dismiss();
                Toast.makeText(getApplicationContext(), Static.ERR_MSG, Toast.LENGTH_LONG).show();
            }
        });


    }

    @OnClick({R.id.bt_chat, R.id.bt_anggota})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_chat:
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
                finish();
                break;
            case R.id.bt_anggota:
                break;
        }
    }
}
