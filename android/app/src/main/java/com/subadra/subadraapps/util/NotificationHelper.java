package com.subadra.subadraapps.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.subadra.subadraapps.R;
import com.subadra.subadraapps.ui.LoginActivity;
import com.subadra.subadraapps.ui.chat.ChatActivity;

public class NotificationHelper {

//    private Context mContext;
//    private NotificationManager mNotificationManager;
//    private NotificationCompat.Builder mBuilder;
//    public static final String NOTIFICATION_CHANNEL_ID = "10001";
//
//    public NotificationHelper(Context context) {
//        mContext = context;
//        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//    }
//
//    /**
//     * Create and push the notification
//     */
//
//    public void createNotification(String title, String message, String id){
//        int NOTIFY_ID;
//        try{
//            NOTIFY_ID = Integer.parseInt(id); // ID of notification
//        }catch (Exception e){
//            NOTIFY_ID = 0;
//        }
//        Intent intent;
//        PendingIntent pendingIntent;
//        NotificationCompat.Builder builder;
//        if (mNotificationManager == null) {
//            mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            NotificationChannel mChannel;
//            try{
//                mChannel = mNotificationManager.getNotificationChannel(id);
//            }catch (Exception e){
//                mChannel = mNotificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID);
//            }
//            if (mChannel == null) {
//                mChannel = new NotificationChannel(id, title, importance);
//                mChannel.enableVibration(true);
//                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
////                mNotificationManager.createNotificationChannel(mChannel);
//            }
//            builder = new NotificationCompat.Builder(mContext, id);
//            intent = new Intent(mContext, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
//            builder.setContentTitle(title)                            // required
//                    .setSmallIcon(R.mipmap.ic_launcher)   // required
//                    .setContentText(message) // required
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setAutoCancel(true)
//                    .setContentIntent(pendingIntent)
//                    .setTicker(message)
//                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
//        }
//        else {
//            builder = new NotificationCompat.Builder(mContext, id);
//            intent = new Intent(mContext, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
//            builder.setContentTitle(title)                            // required
//                    .setSmallIcon(R.mipmap.ic_launcher)   // required
//                    .setContentText(message) // required
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setAutoCancel(true)
//                    .setContentIntent(pendingIntent)
//                    .setTicker(message)
//                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
//                    .setPriority(Notification.PRIORITY_HIGH);
//        }
//        Notification notification = builder.build();
//        mNotificationManager.notify(NOTIFY_ID, notification);
//    }
//
//    public void cancelNotif(){
//        mNotificationManager.cancel(0);
//    }

    private Context mContext;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    public NotificationHelper(Context context) {
        mContext = context;
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

    }

    /**
     * Create and push the notification
     */
    public void createNotification(String title, String message, String idx)
    {
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(mContext , LoginActivity.class);
//        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent, 0);
        //            pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);

        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
//        mBuilder.setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
        mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(message)
                .setBigContentTitle(title)
                .setSummaryText(message))
                .setContentTitle(title)
                .setContentText(message)
//                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        mBuilder.setAutoCancel(true);
        mBuilder.setOngoing(false);
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }

    public void cancelNotif(){
        mNotificationManager.cancel(0);
    }
}