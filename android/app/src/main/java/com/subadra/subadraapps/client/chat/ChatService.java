package com.subadra.subadraapps.client.chat;

import com.subadra.subadraapps.client.model.Response;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;

public interface ChatService {

    @FormUrlEncoded
    @POST("check-number")
    Call<Response> checkNumber(@FieldMap Map<String, String> paramMap);

    @FormUrlEncoded
    @POST("update-profile")
    Call<Response> editProfile(@FieldMap Map<String, String> paramMap);

    @Multipart
    @POST("update-profile")
    Call<Response> editProfile(@Part MultipartBody.Part paramPart, @PartMap Map<String, RequestBody> paramMap);

    @GET("list-member")
    Call<Response> getMember();

    @GET("pengumuman")
    Call<Response> getPengumuman();

    @GET("profile")
    Call<Response> getProfile(@QueryMap Map<String, String> paramMap);

    @GET("list-chat")
    Call<Response> listChat(@QueryMap Map<String, String> paramMap);

    @FormUrlEncoded
    @POST("login")
    Call<Response> login(@FieldMap Map<String, String> paramMap);

    @FormUrlEncoded
    @POST("register")
    Call<Response> register(@FieldMap Map<String, String> paramMap);

    @Multipart
    @POST("register")
    Call<Response> register(@Part MultipartBody.Part paramPart, @PartMap Map<String, RequestBody> paramMap);

    @FormUrlEncoded
    @POST("sent-chat")
    Call<Response> sentChat(@FieldMap Map<String, String> paramMap);

    @FormUrlEncoded
    @POST("change-pwd")
    Call<Response> updatePwd(@FieldMap Map<String, String> paramMap);

}
