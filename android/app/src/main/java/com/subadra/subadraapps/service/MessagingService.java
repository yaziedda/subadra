package com.subadra.subadraapps.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.chat.model.Chat;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.client.model.ResponseNotif;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.ui.chat.ChatActivity;
import com.subadra.subadraapps.ui.chat.ChatModel;
import com.subadra.subadraapps.util.NotificationHelper;
import com.subadra.subadraapps.util.Preferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MessagingService extends FirebaseMessagingService {

    private static final String TAG = "Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Log.d(TAG, "Message data payload: " + remoteMessage.getData().get("data"));
            String json = remoteMessage.getData().get("data");
            Gson gson = new Gson();
//            JsonElement jsonObject = gson.toJsonTree(json);

            ResponseNotif notifModel = new Gson().fromJson(json, ResponseNotif.class);
            if (notifModel.getType() == 1) {
                ChatModel chatModel = gson.fromJson(gson.toJson(notifModel.getData()), ChatModel.class);
                sendNotification(chatModel);
            }
        }

    }

    public void sendNotification(ChatModel chatModel) {
        int requestID = (int) System.currentTimeMillis();

        NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());

        User user = Preferences.getUser(getApplicationContext());

        if(user.getId() != chatModel.getPengirim()){


            notificationHelper.createNotification(chatModel.getNamaLengkap(), chatModel.getPesan(), String.valueOf(requestID));

            Intent intent = new Intent();
            intent.setAction(ChatActivity.BROADCAST_CHAT);
            intent.putExtra(ChatModel.class.getName(), chatModel);
            sendBroadcast(intent);
            System.out.println("Send broadcast");
        }

//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.mipmap.ic_subadra)
//                        .setContentTitle(chatModel.getNamaLengkap())
//                        .setContentText(chatModel.getPesan());
//        Intent resultIntent = new Intent(this, ChatActivity.class);
//        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent resultPendingIntent =
//                PendingIntent.getActivity(
//                        this,
//                        requestID,
//                        resultIntent,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                );
//
//        mBuilder.setContentIntent(resultPendingIntent);
//        mBuilder.setAutoCancel(true);
//        NotificationManager mNotificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.notify(1001, mBuilder.build());
    }
}