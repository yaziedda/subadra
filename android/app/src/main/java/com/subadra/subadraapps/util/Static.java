package com.subadra.subadraapps.util;



public class Static {

    public final static String MyPref = "com.subadra.project";

    public final static String IP = "http://api-sbd.smkmuh-cikampek.sch.id";
    public final static String BASE_URL = IP+"/api/";
    public final static String BASE_URL_IMAGE = IP+"/storage/";


    public final static String LOGIN_KEY = "LOGIN_KEY";
    public final static String USER_DATA = "USER_DATA";
    public final static String MEMBER_MODEL = "MEMBER_MODEL";

    public final static int CHAT_SENDER = 0;
    public final static int CHAT_RECEIVER = 1;

    public final static String ERR_MSG = "Gagal coba lagi";
    public final static String DIALOG_PLEASEWAIT_TITLE = "Loading...";
}
