package com.subadra.subadraapps.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.subadra.subadraapps.HomeActivity;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.util.BaseActivity;
import com.subadra.subadraapps.util.Preferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;
    @BindView(R.id.et_nama)
    TextInputEditText etNama;
    @BindView(R.id.et_bio)
    TextInputEditText etBio;
    @BindView(R.id.et_pwd)
    TextInputEditText etPwd;
    @BindView(R.id.et_pwd_c)
    TextInputEditText etPwdC;
    String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        phone = getIntent().getStringExtra("phone");
        etPhone.setText(phone);
        etPhone.setEnabled(false);
    }

    @OnClick(R.id.bt_login)
    public void onViewClicked() {
        String nama = etNama.getText().toString();
        String bio = etBio.getText().toString();
        String pwd = etPwd.getText().toString();
        String pwdC = etPwdC.getText().toString();


        if(nama.isEmpty() && bio.isEmpty() && pwd.isEmpty() && pwdC.isEmpty()){
            showMessage("Tidak boleh ada yang kosong");
            return;
        }

        if(!pwd.equals(pwdC)){
            showMessage("Password tidak sama !");
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put("username", phone);
        map.put("nama", nama);
        map.put("tanggal_lahir", "1999-01-01");
        map.put("deskripsi", bio);
        map.put("password", pwd);

        showPleasewaitDialog();

        mobileService.register(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                dissmissPleasewaitDialog();
                Response body = response.body();
                if(body != null && body.isStatus()){
                    User user = new Gson().fromJson(new Gson().toJson(body.getData()), User.class);
                    showMessage("Welcome to Subadra, "+user.getNamaLengkap());
                    Preferences.setUser(getApplicationContext(), user);
                    Preferences.setLoginFlag(getApplicationContext(), true);
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                }else{
                    showMessage("Gagal registrasi");
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                dissmissPleasewaitDialog();
                showMessage("Gagal registrasi");
            }
        });
    }
}
