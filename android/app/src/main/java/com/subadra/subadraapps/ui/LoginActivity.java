package com.subadra.subadraapps.ui;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.gson.Gson;
import com.subadra.subadraapps.HomeActivity;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.util.BaseActivity;
import com.subadra.subadraapps.util.Preferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;
    @BindView(R.id.et_pwd)
    TextInputEditText etPwd;
    public static int APP_REQUEST_CODE = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


    }

    @OnClick({R.id.bt_login, R.id.tv_daftar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_login:

                // mengambil data dari input text
                String phone = etPhone.getText().toString();
                String pwd = etPwd.getText().toString();

                // validasi input text bila kosong
                if(phone.isEmpty() || pwd.isEmpty()){
                    showMessage("No Handphone atau Password tidak boleh kosong");
                    return;
                }

                // membuat login dialog
                showPleasewaitDialog();

                // fungsi untuk menjalankan login dan mengambil data ke server
                Map<String, String> map = new HashMap<>();
                map.put("username", phone);
                map.put("password", pwd);

                mobileService.login(map).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        // menghilangkan dialog
                        dissmissPleasewaitDialog();

                        // cek http response 200 atau sukses
                        if(response.isSuccessful()){
                            Response body = response.body();

                            // cek respose yang diberika server dengan status true
                            if (body.isStatus()){

                                // save data login ke dalam session android menggunakan shared preferences
                                User user = new Gson().fromJson(new Gson().toJson(body.getData()), User.class);
                                Preferences.setUser(getApplicationContext(), user);
                                Preferences.setLoginFlag(getApplicationContext(), true);

                                // berpindah ke activity main
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                finish();
                            }else{
                                // menampilkan pesan bahwa no handphone atau password salah
                                showMessage("No Handphone atau Password salah");
                            }
                        }else{
                            // menampilkan pesan bahwa gagal mendapatkan data
                            showMessage("Gagal, sialahkan coba lagi");
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        // menghilangkan dialog
                        dissmissPleasewaitDialog();
                    }
                });
                break;
            case R.id.tv_daftar:
                final Intent intent = new Intent(LoginActivity.this, AccountKitActivity.class);
                AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                        new AccountKitConfiguration.AccountKitConfigurationBuilder(
                                LoginType.PHONE,
                                AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
                // ... perform additional configuration ...
                intent.putExtra(
                        AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                        configurationBuilder.build());
                startActivityForResult(intent, APP_REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
//                showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();

                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0,10));
                }

                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(final Account account) {
                        final PhoneNumber number = account.getPhoneNumber();
                        if (number != null) {
                            final String phoneNumber = number.toString().replaceFirst("\\+62", "0");
                            Map<String, String> params  = new HashMap<String, String>();
                            params.put("username", phoneNumber);
                            showPleasewaitDialog();
                            mobileService.checkNumber(params).enqueue(new Callback<Response>() {
                                @Override
                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                    dissmissPleasewaitDialog();
                                    Response body = response.body();
                                    if (body != null){
                                        if(!body.isStatus()){
                                            showMessage("Nomor anda belum terdaftar di subadra, silahkan registrasi");
                                            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                                            intent.putExtra("phone", phoneNumber);
                                            startActivity(intent);
                                            finish();
                                        }else{
                                            showMessage("Nomor anda "+phoneNumber+" telah terdaftar, silahkan login");
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<Response> call, Throwable t) {
                                    dissmissPleasewaitDialog();
                                }
                            });

                        }else{
                            Log.e("error", "Number null=");
                        }
                    }

                    @Override
                    public void onError(final AccountKitError error) {
                        Log.e("error", "AccountKitError=" + error);
                    }
                });

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
//                goToMyLoggedInActivity();
            }

            // Surface the result to your user in an appropriate way.
//            Toast.makeText(
//                    this,
//                    toastMessage,
//                    Toast.LENGTH_LONG)
//                    .show();
        }
    }
}
