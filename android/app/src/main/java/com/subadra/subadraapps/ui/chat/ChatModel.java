package com.subadra.subadraapps.ui.chat;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.util.Preferences;
import com.subadra.subadraapps.util.Static;

import java.io.Serializable;

public class ChatModel implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pengirim")
    @Expose
    private Integer pengirim;
    @SerializedName("penerima")
    @Expose
    private Integer penerima;
    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("tipe")
    @Expose
    private Integer tipe;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("nama_lengkap")
    @Expose
    private String namaLengkap;

    private int typeMessage;
    private int status = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPengirim() {
        return pengirim;
    }

    public void setPengirim(Integer pengirim) {
        this.pengirim = pengirim;
    }

    public Integer getPenerima() {
        return penerima;
    }

    public void setPenerima(Integer penerima) {
        this.penerima = penerima;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Integer getTipe() {
        return tipe;
    }

    public void setTipe(Integer tipe) {
        this.tipe = tipe;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public int getTypeMessage(Context context) {
        User user = Preferences.getUser(context);
        if(user.getId() == getPengirim()) {
            return Static.CHAT_SENDER;
        }else {
            return Static.CHAT_RECEIVER;
        }
    }

    public void setTypeMessage(int typeMessage) {
        this.typeMessage = typeMessage;
    }

    public int getTypeMessage() {
        return typeMessage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
