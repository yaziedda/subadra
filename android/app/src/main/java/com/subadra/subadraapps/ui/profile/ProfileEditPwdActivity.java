package com.subadra.subadraapps.ui.profile;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;

import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.util.BaseActivity;
import com.subadra.subadraapps.util.Preferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class ProfileEditPwdActivity extends BaseActivity {

    @BindView(R.id.et_old)
    TextInputEditText etOld;
    @BindView(R.id.et_new)
    TextInputEditText etNew;
    @BindView(R.id.et_new_c)
    TextInputEditText etNewC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit_pwd);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bt_simpan)
    public void onViewClicked() {
        String old = etOld.getText().toString();
        String newPwd = etNew.getText().toString();
        String newPwdC = etNewC.getText().toString();

        if(old.isEmpty() && newPwd.isEmpty() && newPwdC.isEmpty()){
            showMessage("Password tidak boleh kosong");
            return;
        }

        if(!newPwd.equals(newPwdC)){
            showMessage("Password baru tidak sama");
            return;
        }

        showPleasewaitDialog();
        Map<String, String> map = new HashMap<>();
        map.put("id", Preferences.getUser(getApplicationContext()).getId().toString());
        map.put("password_old", old);
        map.put("password_new", newPwd);
        mobileService.updatePwd(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                dissmissPleasewaitDialog();
                if(response.isSuccessful()){
                    if(response.body() != null){
                        if(response.body().isStatus()){
                            showMessage(response.body().getData().toString());
                            finish();
                        }else{
                            showMessage(response.body().getData().toString());
                        }
                    }else{
                        showMessage("Password gagal di update");
                    }
                }else{
                    showMessage("Password gagal di update");
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                dissmissPleasewaitDialog();
                showMessage("Password gagal di update");
            }
        });

    }
}
