package com.subadra.subadraapps.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.subadra.subadraapps.R;
import com.subadra.subadraapps.client.model.Response;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.util.BaseActivity;
import com.subadra.subadraapps.util.Preferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class ProfileEditActivity extends BaseActivity {

    @BindView(R.id.et_nama)
    TextInputEditText etNama;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.et_bio)
    TextInputEditText etBio;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.bind(this);

        User user = Preferences.getUser(getApplicationContext());

        etNama.setText(user.getNamaLengkap());
        tvPhone.setText(user.getNoHp());
        etBio.setText(user.getDeskripsi());
    }

    @OnClick({R.id.bt_simpan, R.id.tv_edit_pwd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_simpan:
                final String nama = etNama.getText().toString();
                final String bio = etBio.getText().toString();

                if (nama.isEmpty() && bio.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Nama atau bio tidak boleh kosong", Toast.LENGTH_LONG).show();
                    return;
                }

                showPleasewaitDialog();

                Map<String, String> map = new HashMap<>();
                map.put("id", Preferences.getUser(getApplicationContext()).getId().toString());
                map.put("nama", nama);
                map.put("deskripsi", bio);
                mobileService.editProfile(map).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        if(response.isSuccessful()){
                            if(response.body() != null && response.body().isStatus()){
                                User user = Preferences.getUser(getApplicationContext());
                                user.setDeskripsi(bio);
                                user.setNamaLengkap(nama);
                                Preferences.setUser(getApplicationContext(), user);
                                showMessage("Sukses update");
                                finish();
                            }else{
                                showMessage("Gagal update");
                            }
                        }else{
                            showMessage("Gagal update");
                        }

                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        showMessage("Gagal update");
                    }
                });

                break;
            case R.id.tv_edit_pwd:
                startActivity(new Intent(getApplicationContext(), ProfileEditPwdActivity.class));
                break;
        }
    }
}
