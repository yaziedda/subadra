package com.subadra.subadraapps.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.subadra.subadraapps.R;
import com.subadra.subadraapps.model.User;
import com.subadra.subadraapps.ui.member.MemberModel;
import com.subadra.subadraapps.util.Preferences;
import com.subadra.subadraapps.util.Static;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_edit)
    TextView tvEdit;

    User user;
    MemberModel memberModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        memberModel = (MemberModel) getIntent().getSerializableExtra(Static.MEMBER_MODEL);
        initLoad();

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileEditActivity.class));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        initLoad();
    }

    private void initLoad() {
        if (memberModel != null) {
            tvEdit.setVisibility(View.GONE);
        } else {
            tvEdit.setVisibility(View.VISIBLE);

            user = Preferences.getUser(getApplicationContext());

            tvName.setText(user.getNamaLengkap());
            tvPhone.setText(user.getNoHp());
            tvDesc.setText(user.getDeskripsi());

            String path = Static.BASE_URL_IMAGE + user.getPhoto();
            Picasso.with(getApplicationContext()).load(path).into(ivProfile);
        }
    }

    @OnClick(R.id.ll_edit)
    public void onViewClicked() {
        startActivity(new Intent(getApplicationContext(), ProfileEditActivity.class));

    }
}
