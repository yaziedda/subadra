package com.subadra.subadraapps.client.chat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class Chat {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pengirim")
    @Expose
    private Integer pengirim;
    @SerializedName("penerima")
    @Expose
    private Integer penerima;
    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("tipe")
    @Expose
    private Integer tipe;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("nama_lengkap")
    @Expose
    private String namaLengkap;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPengirim() {
        return pengirim;
    }

    public void setPengirim(Integer pengirim) {
        this.pengirim = pengirim;
    }

    public Integer getPenerima() {
        return penerima;
    }

    public void setPenerima(Integer penerima) {
        this.penerima = penerima;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Integer getTipe() {
        return tipe;
    }

    public void setTipe(Integer tipe) {
        this.tipe = tipe;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }
}
